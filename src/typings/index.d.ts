declare module '@tundra-ui/core' {
    import { Plugin } from 'vue';

    export function install(): Plugin;
}
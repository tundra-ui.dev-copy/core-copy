import { DefineComponent, Plugin } from 'vue';

const modules = import.meta.globEager('./**/index.ts');
const components: { [name: string]: DefineComponent } = {};

for (const path in modules) {
    const component: DefineComponent = modules[path].default;

    if (component.name) {
        components[component.name] = component;
    }
}

const plugin: Plugin = {
    install(app) {
        for (const name in components) {
            app.component(name, components[name]);
        }
    }
}

export default plugin;
